function bool = reproduceOrNot(R,a,i,j)

if i ~= j
    if (R(i) < 1 || R(j) < 1) 
        bool = 0;
    else
        prob = (R(j)/(a + R(j)))*(R(i)/(a + R(i)));
        %X = ['R(i): ', num2str(R(i)),' R(j): ',num2str(R(j))];
        %disp(X)
        bool = rand < prob;
    end
else
    if (R(i) < 2) 
        bool = 0;
    else
        prob = (R(i)/(a + R(i)))*((R(i)-1)/(a + R(i) - 1));
        
        %X = ['R(i): ', num2str(R(i)),' R(j): ',num2str(R(j))];
        %disp(X)
        bool = rand < prob;
    end
end

%X = ['prob: ', num2str(prob),' bool: ',num2str(bool)];
%disp(X)




