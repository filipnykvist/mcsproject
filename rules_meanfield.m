function count = rules_meanfield(i,k,l,n)
count = 0;
added = k + l;
if added < (n+1) & i == added
    count = 1;
elseif added == (n+1) & i == 1
    count = 1;
elseif added > (n+1)
    if (i == 1)
        count = 1;
    end
    index = mod(added, n+1);
    if (i == index)
        count = count + 1;
    end
end