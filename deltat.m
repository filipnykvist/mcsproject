% Generate a dt from
%   P(tau <= dt <= dtau) = N*exp(-N*tau)*dtau
% order of change can be decided by altering dtau

function dT = deltaT(N,t,dTau)
dT = N*exp(-N*t)*dTau;
 
    