function [R,S, t, timeIndex, N] =setIC(dimX, dimY)
R = zeros(dimX,1);
S = ones(dimX,dimY);
S = S + 2*triu(S);
S = S-2;

t = 0;%0.0001;
timeIndex = 1;
N = sum(S(S~=-1));