%(i)
clear all;
close all;

% Parameter values
n = 3;
dimX = n;%=i
dimY = n;%=j

%mu = 2000;
mu = 500;
a = 5;
p = 0.001;
indexR = 1;

endTime = 1000; 

bigR = zeros(dimX, endTime);
bigS = zeros(dimX*dimY,1);

% Initial conditions
[R,S,t,timeIndex,N] = setIC(dimX,dimY);
bigR_meanfield = zeros(dimX, endTime);
bigS_meanfield = zeros(dimX*dimY,1);
% save ICs
count = 1;
bigR(:,timeIndex) = R;
S_meanfield = S;
R_meanfield = R;
bigR_meanfield(:,count) = R_meanfield;
for i = 1:dimX
    for j = 1:dimY
        %disp(i+dimY*(j-1))
        bigS_meanfield(i+dimY*(j-1),count) = S_meanfield(i,j);
        bigS(i+dimY*(j-1),count) = S(i,j);
    end
end
count = count + 1;
%debug_RepOrNot = zeros(0,5)
% Iterate through
while t(end) <= endTime
    %disp(t(end))
    N = sum(sum(S(S~=-1)));
    N_meanfield = sum(S_meanfield(S_meanfield~=-1));

    if N == 0
        break;
    end
    
    %(ii)
    %dT = -log(rand)/N;
    dT = 1/N;
    %dT = exprnd(1/N);
    t(end+1) = t(end) + dT;
    %disp(dT)
    
    %(iii)
    R = inflow(dT,mu,R, indexR); 
   
    %(iv)
    [selectOrgX,selectOrgY] = selectOrganism(dimX, dimY,S);
    
    %(v)
    if(reproduceOrNot(R,a,selectOrgX,selectOrgY))    
        [R,S] = rules(R,S,selectOrgX,selectOrgY,dimX,dimY,p);
    else
        S = dies(selectOrgX,selectOrgY,S);
    end
    
    %R_meanfield = meanfield_R(R_meanfield,S_meanfield,N_meanfield,a,mu,n,indexR);
    %S_meanfield = meanfield_S(S_meanfield,R_meanfield,N_meanfield,p,n,a);
        %if selectOrgX == 2 ||  selectOrgY == 2
        %  disp('hej')  
        %end
    bigR(:,count) = R;
    %bigR_meanfield(:,count) = R_meanfield;
    for i = 1:dimX
        for j = 1:dimY
            bigS(i+dimY*(j-1),count) = S(i,j);
            %bigS_meanfield(i+dimY*(j-1),count) = S_meanfield(i,j);
        end
    end
    count  = count + 1;
end
interval = int32(count/endTime);
stT = t(1:interval:end);
stR = bigR(:,1:interval:end);
stS = bigS(:,1:interval:end);
start = 30;

%MFstR = bigR_meanfield(:,1:interval:end);
%MFstS = bigS_meanfield(:,1:interval:end);
if n == 3
    Splot = stS([1 4 5 7 8 9],:);
    Splot = Splot(:,start:end);
    stT = stT(start:end);
    %mfSplot = MFstS([1 4 5 7 8 9],:);
elseif n == 2
    Splot = stS([1 3 4],:);
    Splot = Splot(:,start:end);
    stT = stT(start:end);
    %mfSplot = MFstS([1 3 4],:);
end
% figure
% subplot(2,2,1);
% plot(stT,stR)
% subplot(2,2,2);
% area(stT,Splot')
% subplot(2,2,3);
% plot(stT,MFstR)
% subplot(2,2,4);
%area(stT,mfSplot')


figure
area(stT,Splot')
legend('s_{11}','s_{12}','s_{22}')


% figure
% plot(stT,Splot')
% plot(t,bigS)
% figure
% plot(t,bigR)


