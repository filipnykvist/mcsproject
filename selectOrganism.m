%SelectOrganism (iv) in the final project
%returns x and y index for the S matrix.

function [i, j] = selectOrganism(dimX, dimY, S)
% species is a row vector containing all elements of S row-wise
species = S;
species(species==-1) = 0;
species = species';
species = species(:)';

% Probability of picking a species is proportional to S_ij/N
pick_probability = cumsum(species)./sum(species);
index = find(pick_probability > rand);
index = index(1);

% Find corresponding species in S
i = ceil(index/dimY);
j = mod(index,dimY);
if (j == 0)
    j = dimY;
end
% i = randi(dimX);
% j = randi(dimY);
% while(S(i,j) <= 0)
%     i = randi(dimX);
%     j = randi(dimY);
% end
%X = ['selected organism -- i: ', num2str(i) , ' j: ' ,num2str(j)];
%disp(X)