function S_meanfield_new = meanfield_S(S_meanfield,R,N,p,n,a)

sum = 0;
for i = 1:n
    for j = 1:n
        if S_meanfield(i,j) ~= -1
            sum = sum + S_meanfield(i,j)*reproduceOrNot(R,a,i,j);
        end
    end
end
sum = sum*p/(N*(n-1));

for i = 1:n
    for j = 1:n
        if S_meanfield(i,j) ~= -1
            S_meanfield_new(i,j) = S_meanfield(i,j) ...
                + S_meanfield(i,j)/N*reproduceOrNot(R,a,i,j)*(1-p) ...
                - S_meanfield(i,j)/N*(1-reproduceOrNot(R,a,i,j)) ...
                + sum - p/(N*(n-1))*S_meanfield(i,j)*reproduceOrNot(R,a,i,j);
        end
    end
end