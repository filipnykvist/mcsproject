function R_meanfield_new = meanfield_R(R_meanfield,S_meanfield,N,a,mu,n,indexR)
R_meanfield_new = R_meanfield;
for i = 1:n
    for j = 1:n
        if S_meanfield(i,j) ~= -1
            R_meanfield_new(i) = R_meanfield_new(i) ...
                - S_meanfield(i,j)/N*reproduceOrNot(R_meanfield,a,i,j)*(1+(i==j));
        end
    end
    for k = 1:n
        for l = 1:n
            if S_meanfield(k,l) ~= -1
                R_meanfield_new(i) = R_meanfield_new(i) ...
                    + S_meanfield(k,l)/N*reproduceOrNot(R_meanfield,a,k,l)*rules_meanfield(i,k,l,n);
            end
        end
    end
    R_meanfield_new(i) = R_meanfield_new(i) + (indexR==i)*(1/N)*mu;
end

% if R_meanfield_new(2) < 0
%     S_meanfield(2,3)*reproduceOrNot(R_meanfield,a,2,3)*(1+(2==3))
% end