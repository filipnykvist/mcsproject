function [Rnew, Snew] = rules(R,S, selectOrgx, selectOrgy, dimX,dimY,p)
added = selectOrgx + selectOrgy;
Rnew = R;
Snew = S;
%disp('REPRODUCE')
if added<(dimX+1)
    Rnew(added) = R(added) +1;
elseif added == (dimX+1)
    Rnew(1) = R(1) +1;
elseif added > (dimX+1)
    Rnew(1) = R(1) +1;
    index = mod(added, dimX+1);
    Rnew(index) = Rnew(index)+1;
    
end
Rnew(selectOrgx) = Rnew(selectOrgx)-1;
Rnew(selectOrgy) = Rnew(selectOrgy)-1;
if rand > p
    Snew(selectOrgx, selectOrgy) = Snew(selectOrgx, selectOrgy)+1;
else
    %disp('MUTATION')
    i = randi(dimX);
    j = randi(dimY);
    while (Snew(i,j) < 0)  || ((i == selectOrgx && j == selectOrgy))
        i = randi(dimX);
        j = randi(dimY);
    end
    Snew(i, j) = Snew(i, j) + 1;  
end
