function [] = plotter(bigS,bigR,dimX, dimY, endTime)


figure
for i = 1:dimX
    for j = 1:dimY
        for k = 1:endTime
            v(k) = bigS(i,j,k);
        end
        plot(v,'Linewidth',2);
        hold on
    end
end

hold off
%legendcell = cellstr(num2str(bigS,'s = %-11.4g'));
%legend(legendcell);
set(gca,'Fontsize', 16)
xlabel('generation, t')
ylabel('no. organisms')

figure
for j = 1:dimX
    plot(bigR(j,:),'Linewidth',2);
    hold on
end
hold off

%legendcell = cellstr(num2str(bigR','r = %-11.4g'));
%legend(legendcell);
set(gca,'Fontsize', 16)
xlabel('generation, t')
ylabel('no. metabolites')
