function [bigR, bigS] = SaveData(bigR, bigS, R,S, timeIndex)
bigR(:,timeIndex) = R;
bigS(:,:,timeIndex) = S;