% inflow of metabolites
% parameter value mu 
function R = inflow(dt,mu,R, indexR)
R(indexR) = R(indexR) + dt*mu; 